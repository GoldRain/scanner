package edu.sfsu.cs.orange.ocr.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import edu.sfsu.cs.orange.ocr.R;


@SuppressLint("ValidFragment")
public class PicFromFragment extends Fragment {

    MainActivity _activity;
    View view;

    EditText edt_phone;
    TextView txv_scan, txv_call;

    public PicFromFragment(MainActivity activity) {
        // Required empty public constructor

        this._activity = activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_pic_from, container, false);

        loadLayout();
        return view;
    }

    private void loadLayout() {

        edt_phone = (EditText)view.findViewById(R.id.edt_phone);


        txv_call = (TextView)view.findViewById(R.id.txv_call);
        txv_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(_activity, "Call now", Toast.LENGTH_SHORT).show();
            }
        });

        txv_scan = (TextView)view.findViewById(R.id.txv_scan);
        txv_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _activity.gotoScanner();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (Constant.OCR_RESULT.length() != 0){

            edt_phone.setText(Constant.OCR_RESULT);

        } else {

            edt_phone.setText("");
        }
    }
}
