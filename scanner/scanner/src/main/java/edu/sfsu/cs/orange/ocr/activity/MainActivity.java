package edu.sfsu.cs.orange.ocr.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import edu.sfsu.cs.orange.ocr.CaptureActivity;
import edu.sfsu.cs.orange.ocr.R;

import static edu.sfsu.cs.orange.ocr.R.id.fly_container;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    DrawerLayout ui_drawerlayout;
    ImageView ui_imv_call_draw;
    TextView txv_title;
    LinearLayout lyt_from_pic_nav, lyt_ratting_us_nav, lyt_call_us_nav, lyt_share_us_nav;   //nav

    String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE,  android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_SMS,
            android.Manifest.permission.CAMERA, Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.RECORD_AUDIO, Manifest.permission.BIND_VOICE_INTERACTION};

    int MY_PEQUEST_CODE = 107;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkAllPermission();
        loadLayout();
    }

    /*==================== Permission========================================*/
    public void checkAllPermission() {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (hasPermissions(this, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }
    }

    /*//////////////////////////////////////////////////////////////////////////////*/

    public boolean hasPermissions(Context context, String... permissions) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {

            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {

                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_PEQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        }


    }

    private void loadLayout() {

        txv_title = (TextView)findViewById(R.id.txv_title);

        ui_drawerlayout = (DrawerLayout)findViewById(R.id.drawerlayout);

        ui_imv_call_draw = (ImageView)findViewById(R.id.imv_call_drawer);
        ui_imv_call_draw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDrawer();
            }
        });


        /*NAVIGATION*/
        lyt_from_pic_nav = (LinearLayout)findViewById(R.id.lyt_from_pic_nav);
        lyt_from_pic_nav.setOnClickListener(this);

        lyt_ratting_us_nav = (LinearLayout)findViewById(R.id.lyt_ratting_us_nav);
        lyt_ratting_us_nav.setOnClickListener(this);

        lyt_call_us_nav = (LinearLayout)findViewById(R.id.lyt_call_us_nav);
        lyt_call_us_nav.setOnClickListener(this);

        lyt_share_us_nav = (LinearLayout)findViewById(R.id.lyt_share_us_nav);
        lyt_share_us_nav.setOnClickListener(this);

        gotoFromFragment();
        ui_drawerlayout.closeDrawers();

    }

    public void showDrawer() {

        ui_drawerlayout =(DrawerLayout)findViewById(R.id.drawerlayout);

        ui_drawerlayout.openDrawer(Gravity.LEFT);
    }

    public void gotoFromFragment(){

        txv_title.setText(getString(R.string.from_pic));

        PicFromFragment fragment = new PicFromFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(fly_container, fragment).commit();

    }

    public void gotoRattingFragment(){

        txv_title.setText(getString(R.string.ratting_us));

        RattingFragment fragment = new RattingFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(fly_container, fragment).commit();
    }

    public void gotoCallFragement(){

        txv_title.setText(getString(R.string.call_us));

        CallUsFragment fragment = new CallUsFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(fly_container, fragment).commit();
    }

    public void gotoShareFragment(){

        txv_title.setText(getString(R.string.share_us));

        ShareFragment fragment = new ShareFragment(this);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(fly_container, fragment).commit();
    }

    public void gotoScanner(){

        startActivity(new Intent(this, CaptureActivity.class));
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.lyt_from_pic_nav:
                gotoFromFragment();
                ui_drawerlayout.closeDrawers();
                break;

            case R.id.lyt_ratting_us_nav:
                gotoRattingFragment();
                ui_drawerlayout.closeDrawers();
                break;

            case R.id.lyt_call_us_nav:
                gotoCallFragement();
                ui_drawerlayout.closeDrawers();
                break;

            case R.id.lyt_share_us_nav:
                gotoShareFragment();
                ui_drawerlayout.closeDrawers();
                break;
        }

    }

}
