package edu.sfsu.cs.orange.ocr.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import edu.sfsu.cs.orange.ocr.R;


@SuppressLint("ValidFragment")
public class RattingFragment extends Fragment {

    MainActivity _activity;
    View view;

    public RattingFragment(MainActivity activity){

        this._activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_ratting, container, false);

        loadLayout();
        return view;
    }

    private void loadLayout() {


    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (MainActivity)context;
    }
}
